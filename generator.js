const mysql = require(`mysql`);
const config = require(`./config.js`);

let counter = 0;

let pool = mysql.createPool({
    host: config.get(`mysql.host`),
    user: config.get(`mysql.user`),
    password: config.get(`mysql.password`),
    database: config.get(`mysql.name`)
})

setInterval(() => {
    pool.query(`INSERT INTO measurements(Name,Value,Timestamp) VALUES ('metric${counter++}',${Math.random() * 800},${new Date().getTime()});`, (error, result, field) => {
        if (error) {
            console.error(`[ERROR] ${new Date()} insert measurment: ${error.stack}`);
            return;
        }
        console.error(`[INFO] ${new Date()} success insert measurments`);
    })
}, config.get(`generator_interval`))


process.on(`SIGTERM`, shutDown);
process.on(`SIGINT`, shutDown);

function shutDown() {
    process.exit();
}
