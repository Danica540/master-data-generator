const convict = require(`convict`);

const config = convict({
    polly_retries: {
        doc: `Number of retries.`,
        format: Number,
        default: 5,
        env: `POLLY_RETRIES`,
        arg: `polly_retriess`
    },
    generator_interval: {
        doc: `Time inverval between two generated values in ms`,
        format: Number,
        default: 2000,
        env: `GENERATOR_INTERVAL`,
        arg: `generator_interval`
    },
    mysql: {
        host: {
            doc: `Database host name/IP`,
            format: `*`,
            default: `localhost`,
            env: `DB_HOST`,
            arg: `db-host`
        },
        name: {
            doc: `Database name`,
            format: String,
            default: `demo_db`,
            env: `DB_NAME`,
            arg: `db-name`
        },
        user: {
            doc: `Database username`,
            format: String,
            default: `root`,
            env: `DB_USER`,
            arg: `db-user`
        },
        password: {
            doc: `Database password`,
            format: String,
            default: `root`,
            env: `DB_PASSWORD`,
            arg: `db-password`
        }
    }
});

module.exports = config;
