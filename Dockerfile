FROM node:12

WORKDIR /app
COPY package.json ./
COPY package-lock.json ./

RUN npm install

COPY generator.js config.js ./

ENTRYPOINT [ "node" ,"generator.js" ]